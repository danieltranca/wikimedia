﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class GameController : SingletonBehaviour<GameController>
{
    public void Start()
    {
        LocationController.Instance.ListenToUserPosition((userPosition) =>
        {
            GetEntries(userPosition);
            LocationController.Instance.ListenToConditionUserPosition(GlobalVars.Instance.GeoSearchRefresh, GetEntries);
        }, UpdateEntries);
    }

    private void UpdateEntries(Vector2 userPosition)
    {
        ContentController.Instance.UpdateDistances();
    }

    private void GetEntries(Vector2 userPosition)
    {
        WikiOperations.Instance.GetLocalAttractions(userPosition, ContentController.Instance.Fill);
    }
}
