﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ContentController : SingletonBehaviour<ContentController>
{
    [SerializeField]
    private int m_maxEntries = 15;
    [SerializeField]
    private EntryBehaviour m_entryTemplate;
    [SerializeField]
    private VerticalLayoutGroup m_layoutGroup;

    private Dictionary<int, EntryBehaviour> m_entries;
    private ObjectPool<EntryBehaviour> m_entriesPool;
    private LTDescr m_rearrangeDescr;

    public void Awake()
    {
        m_entries = new Dictionary<int, EntryBehaviour>();
        m_entriesPool = new ObjectPool<EntryBehaviour>(m_entryTemplate, 10, (entry) =>
         {
             entry.transform.SetParent(transform);
             entry.GetComponent<RectTransform>().Reset();
         })
        {
            OnObjectUsed = (entry) =>
            {
                entry.Show();
            },
            OnObjectRevoked = (entry) =>
            {
                Debug.Log(entry.name);
                entry.Hide();
            }
        };
    }

    public void Fill(List<GeoSearchItem> items)
    {
        UpdateEntries(items);
        LeanTween.delayedCall(GlobalVars.Instance.AnimationTime, Rearrange);
    }

    public void UpdateDistances()
    {
        foreach (KeyValuePair<int, EntryBehaviour> kvp in m_entries)
            kvp.Value.UpdateDistance();
        LeanTween.delayedCall(GlobalVars.Instance.AnimationTime, Rearrange);
    }

    private void UpdateEntries(List<GeoSearchItem> items)
    {
        List<EntryBehaviour> toRevoke = m_entries.Values.ToList();
        List<GeoSearchItem> toAdd = new List<GeoSearchItem>();
        for (int i = 0; i < items.Count; i++)
        {
            GeoSearchItem item = items[i];
            if (m_entries.ContainsKey(item.PageId))
            {
                m_entries[item.PageId].Fill(item);
                toRevoke.Remove(m_entries[item.PageId]);
            }
            else
            {
                toAdd.Add(item);
            }
        }

        for (int i = 0; i < toAdd.Count; i++)
        {
            GeoSearchItem item = toAdd[i];
            EntryBehaviour entry = m_entriesPool.Get();
            entry.Fill(item);
            m_entries.Add(item.PageId, entry);
        }

        for (int i = 0; i < toRevoke.Count; i++)
        {
            EntryBehaviour entry = toRevoke[i];
            m_entriesPool.Revoke(entry);
            m_entries.Remove(entry.Item.PageId);
        }
    }

    private void Rearrange()
    {
        if (m_rearrangeDescr != null)
            LeanTween.cancel(m_rearrangeDescr.id);
        List<EntryBehaviour> active = m_entries.Values.Where(e => e.gameObject.activeInHierarchy).ToList();
        List<EntryBehaviour> ordered = active.OrderBy(e => e.Item.Distance).ToList();
        List<float> oldPositions = ordered.Select(e => e.RectTransform.anchoredPosition.y).ToList();

        m_layoutGroup.enabled = false;
        for (int i = 0; i < ordered.Count; i++)
        {
            ordered[i].transform.SetSiblingIndex(ordered.Count - 1 - i);
        }

        m_rearrangeDescr = LeanTween.value(gameObject, (val) =>
        {
            for (int i = 0; i < ordered.Count; i++)
            {
                ordered[i].RectTransform.anchoredPosition = new Vector2(ordered[i].RectTransform.anchoredPosition.x, Mathf.Lerp(oldPositions[i], -i * GlobalVars.Instance.EntryHeight, val));
            }
        }, 0, 1, GlobalVars.Instance.AnimationTime).setEaseInOutQuart().setOnComplete(() =>
        {
            for (int i = 0; i < ordered.Count; i++)
            {
                ordered[i].transform.SetSiblingIndex(i);
            }
            m_layoutGroup.enabled = true;
        });
    }
}
