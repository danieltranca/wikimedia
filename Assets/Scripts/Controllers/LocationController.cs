﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocationController : SingletonBehaviour<LocationController>
{
    private const int k_earthRadius = 6371;

#if UNITY_EDITOR
    [SerializeField]
    private Vector2 m_userPosition;//51.5195 | -0.1025f
#endif
    private bool m_wasUserPositionFound;

    private Vector2 m_lastUserPosition;
    private Dictionary<int, Vector2> m_lastUserConditionalPosition;

    private Action<Vector2> m_onLocationUpdated;
    private Dictionary<int, Action<Vector2>> m_onConditionalLocationUpdated;

    public void Awake()
    {
        m_lastUserConditionalPosition = new Dictionary<int, Vector2>();
        m_onConditionalLocationUpdated = new Dictionary<int, Action<Vector2>>();
    }

    public void Update()
    {
        UpdateLocation();
    }

    private void UpdateLocation()
    {
        if (!m_wasUserPositionFound)
            return;

#if UNITY_EDITOR
        Vector2 userPosition = m_userPosition;
#else
        Vector2 userPosition = new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude);
#endif
        if (Math.Abs(m_lastUserPosition.x - userPosition.x) >= GlobalVars.Instance.FloatCompareEpsilon ||
            Math.Abs(m_lastUserPosition.y - userPosition.y) >= GlobalVars.Instance.FloatCompareEpsilon)
        {
            m_lastUserPosition = userPosition;
            if (m_onLocationUpdated != null)
                m_onLocationUpdated(m_lastUserPosition);

            List<int> keys = m_lastUserConditionalPosition.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                Vector2 value = m_lastUserConditionalPosition[keys[i]];
                if (GetDistance(value.x, value.y) > keys[i])
                {
                    m_lastUserConditionalPosition[keys[i]] = m_lastUserPosition;
                    m_onConditionalLocationUpdated[keys[i]](m_lastUserPosition);
                }
            }
        }
    }


    /// <summary>
    /// Listens to condition user position. You can only subscribe to this if the user position was found
    /// </summary>
    /// <param name="distance">Distance.</param>
    /// <param name="onLocationUpdated">On location updated.</param>
    public void ListenToConditionUserPosition(int distance, Action<Vector2> onLocationUpdated)
    {
        if (!m_wasUserPositionFound)
            return;
        m_lastUserConditionalPosition.Add(distance, m_lastUserPosition);
        m_onConditionalLocationUpdated.Add(distance, onLocationUpdated);
    }

    public void ListenToUserPosition(Action<Vector2> onLocationFound, Action<Vector2> onLocationUpdated)
    {
        StartCoroutine(ListenToUserPositionCoroutine(onLocationFound, onLocationUpdated));
    }

    public IEnumerator ListenToUserPositionCoroutine(Action<Vector2> onLocationFound, Action<Vector2> onLocationUpdated)
    {
        yield return new WaitForEndOfFrame();
        m_onLocationUpdated = onLocationUpdated;

#if UNITY_EDITOR

        m_lastUserPosition = m_userPosition;

        if (onLocationFound != null)
        {
            m_wasUserPositionFound = true;
            onLocationFound(m_lastUserPosition);
        }
#else

        if (Input.location.status == LocationServiceStatus.Stopped)
        {
            Input.location.Start(1f, 1f);//Recommended values are 5/5 but due to requirements we'll pick 1/1
            Input.compass.enabled = true;
        }

        int maxWait = 5;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait < 1)
        {
            Debug.LogWarning("Timed out");
            yield break;
        }

        if (Input.location.status != LocationServiceStatus.Running)
        {
            Debug.LogWarningFormat("Unable to determine device location", Input.location.status);
            yield break;
        }
        else
        {
            m_lastUserPosition = new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude);
            if (onLocationFound != null)
            {
                m_wasUserPositionFound = true;
                onLocationFound(m_lastUserPosition);
            }
        }

#endif
    }

    public float GetDistance(float lat, float lon)
    {
        if (!m_wasUserPositionFound)
            return 0;
        var userLat = Mathf.Deg2Rad * m_lastUserPosition.x;
        var destLat = Mathf.Deg2Rad * lat;
        var distLat = Mathf.Deg2Rad * (lat - m_lastUserPosition.x);
        var distLon = Mathf.Deg2Rad * (lon - m_lastUserPosition.y);
        var flatDist = Mathf.Pow(Mathf.Sin(distLat / 2), 2) + (Mathf.Pow(Mathf.Sin(distLon / 2), 2) * Mathf.Cos(userLat) * Mathf.Cos(destLat));
        var circ = 2 * Mathf.Atan2(Mathf.Sqrt(flatDist), Mathf.Sqrt(1 - flatDist));
        return k_earthRadius * circ * 1000; // convert to meters
    }
}
