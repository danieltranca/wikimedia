﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public static class ComponentExtensions
{
	public static void AddComponents (this GameObject gameObject, List<Type> componentTypes)
	{
		componentTypes.ForEach (c => gameObject.AddComponent (c));
	}

	public static T GetComponentInChildrenAll<T> (this GameObject gameObject) where T : Component
	{
		T result = gameObject.GetComponent<T> ();                
		
		if (null == result) {
			Transform trnsRoot = gameObject.transform;
			int iNumChildren = trnsRoot.childCount;
			
			for (int i = 0; i < iNumChildren; i++) {
				result = trnsRoot.GetChild (i).gameObject.GetComponentInChildrenAll<T> ();
				if (null != result) 
					break;
			}
		}
		return result;
	}


	public static void Reset (this RectTransform rectTransform)
	{
		rectTransform.Reset (Anchor.Current);
	}

	public static void Reset (this RectTransform rectTransform, Anchor anchor)
	{
		rectTransform.transform.Reset ();
		rectTransform.anchoredPosition = new Vector2 (0, 0);
		
		switch (anchor) {
		case Anchor.MiddleCenter:
			rectTransform.anchorMin = new Vector2 (0.5f, 0.5f);
			rectTransform.anchorMax = new Vector2 (0.5f, 0.5f);
			break;
		case Anchor.MiddleLeft:
			rectTransform.anchorMin = new Vector2 (0f, 0.5f);
			rectTransform.anchorMax = new Vector2 (0f, 0.5f);
			break;
		case Anchor.StrechStrech:
			rectTransform.anchorMin = new Vector2 (0, 0);
			rectTransform.anchorMax = new Vector2 (1, 1);
			rectTransform.sizeDelta = new Vector2 (0, 0);
			break;
		case Anchor.BottomStretch:
			rectTransform.anchorMin = new Vector2 (0, 0);
			rectTransform.anchorMax = new Vector2 (1, 0);
			break;
		case Anchor.BottomCenter:
			rectTransform.anchorMin = new Vector2 (0.5f, 0);
			rectTransform.anchorMax = new Vector2 (0.5f, 0);
			break;
		default:
			if (rectTransform.anchorMin == new Vector2 (0, 0) && rectTransform.anchorMax == new Vector2 (1, 1))
				rectTransform.sizeDelta = new Vector2 (0, 0);
			break;
		}
	}

	public static void Reset (this Transform transform)
	{
		transform.localPosition = new Vector3 (0, 0, 0);
		transform.localScale = new Vector3 (1, 1, 1);
		transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, 0));
	}

	public static void RemoveChildren (this Transform transform)
	{
		foreach (Transform child in transform) {
			GameObject.Destroy (child.gameObject);
		}
	}

	public static GameObject AddEmptyUIGameObject (this Component parent, string name = "UIGameObject")
	{
		GameObject gameObject = new GameObject ();
		gameObject.transform.SetParent (parent.transform);
		gameObject.name = name;
		gameObject.AddComponents (new List<System.Type>{typeof(RectTransform), typeof(CanvasRenderer)});
		gameObject.GetComponent<RectTransform> ().Reset (Anchor.StrechStrech);
		
		return gameObject;
	}

	public static IEnumerator WaitForAnimation (this Animator animator, string animationName, Action callback = null, string baseLayer = "Base Layer", float normalizedTime = 1f)
	{
		AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo (0);
		while (!animatorStateInfo.IsName(baseLayer + "." + animationName) || animatorStateInfo.normalizedTime <= normalizedTime) {
			animatorStateInfo = animator.GetCurrentAnimatorStateInfo (0);
			yield return new WaitForSeconds (0.1f);
		}
		if (callback != null)
			callback ();
	}

	public static void ChangeLayersRecursively (this GameObject gameObject, string layer)
	{
		gameObject.ChangeLayersRecursively (LayerMask.NameToLayer (layer));
	}

	public static void ChangeLayersRecursively (this GameObject gameObject, int layer)
	{
		try {
			gameObject.layer = layer;
			foreach (Transform child in gameObject.transform) {
				child.gameObject.layer = layer;
				ChangeLayersRecursively (child.gameObject, layer);
			}
		} catch (Exception ex) {
			Debug.Log (ex.ToString ());    
		}
	}

	public static List<T> GetComponentsOfTypeRecursevely<T> (this GameObject gameObject)
	{
		try {
			return gameObject.transform.GetComponentsOfTypeRecursevely<T> ();
		} catch (Exception ex) {
			Debug.LogError (gameObject.name);
			throw ex;
		}
	}

	public static List<T> GetComponentsOfTypeRecursevely<T> (this Transform transform)
	{
		List<T> result = transform.GetComponents<T> ().ToList ();
		if (transform.childCount != 0)
			foreach (Transform child in transform)
				result.AddRange (child.GetComponentsOfTypeRecursevely<T> ());
		return result;
	}
}

public enum Anchor
{
	BottomLeft,
	BottomCenter,
	BottomRight,
	BottomStretch,
	MiddleLeft,
	MiddleCenter,
	MiddleRight,
	MiddleStrech,
	TopLeft,
	TopCenter,
	TopRight,
	TopStrech,
	StrechLeft,
	StrechCenter,
	StrechRight,
	StrechStrech,
	Current
}