﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class GeoSearchItem
{
    public int PageId { get; set; }

    public string Title { get; set; }

    public float Latitute { get; set; }

    public float Longitude { get; set; }

    public int Distance { get; set; }

    public string Url { get; set; }

    public static GeoSearchItem Read(JSONNode node)
    {
        GeoSearchItem item = new GeoSearchItem();
        item.PageId = node["pageid"].AsInt;
        item.Title = node["title"];
        item.Latitute = node["lat"].AsFloat;
        item.Longitude = node["lon"].AsFloat;
        return item;
    }

    public override string ToString()
    {
        return string.Format("[{0} - {1}|{2}]", Title, Latitute, Longitude);
    }
}
