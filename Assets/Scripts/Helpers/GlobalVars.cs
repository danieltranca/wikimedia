﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVars : Singleton<GlobalVars>
{
    private const float k_floatCompareEpsilon = 0.00001f;

    public float FloatCompareEpsilon { get { return k_floatCompareEpsilon; } }

    private const int k_geoSearchRefresh = 20;

    public int GeoSearchRefresh { get { return k_geoSearchRefresh; } }

    private const int k_entryHeight = 150;

    public int EntryHeight { get { return k_entryHeight; } }

    private const float k_animationTime = 0.3f;

    public float AnimationTime { get { return k_animationTime; } }
}
