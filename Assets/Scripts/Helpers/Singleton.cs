﻿using UnityEngine;
using System.Collections;

public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    private static T s_instance;
    private static object s_lock = new object();

    public static bool HasInstance
    {
        get
        {
            ReadInstance();
            return s_instance != null;
        }
    }

    public static T Instance
    {
        get
        {
            ReadInstance();
            return s_instance;
        }
    }

    protected static void ReadInstance()
    {
        lock (s_lock)
        {
            if (s_instance == null)
            {
                s_instance = (T)FindObjectOfType(typeof(T));

                if (s_instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogWarning("[Singleton] An instance of " + typeof(T) + " is needed in the scene. Please create one before accesing it.");
#endif
                }
            }
        }
    }

    protected void ResetInstance()
    {
        s_instance = null;
    }

    public void OnDestroy()
    {
        ResetInstance();
    }

}

public class Singleton<T> where T : new()
{
    private static T s_instance;
    private static object s_lock = new object();

    public static bool HasInstance
    {
        get
        {
            ReadInstance();
            return s_instance != null;
        }
    }

    public static T Instance
    {
        get
        {
            ReadInstance();
            return s_instance;
        }
    }

    protected static void ReadInstance()
    {
        lock (s_lock)
        {
            if (s_instance == null)
            {
                s_instance = new T();
            }
        }
    }
}
