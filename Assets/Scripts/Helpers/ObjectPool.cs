﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Object = UnityEngine.Object;

public class ObjectPool<T> where T : Object
{

	private T m_object;
	private int m_count;
	private List<PoolModel> m_list;
    private Action<T> m_onObjectAdded;
    public Action<T> OnObjectAdded { get { return m_onObjectAdded; } set { m_onObjectAdded = value; } }

    private Action<T> m_onObjectRevoked;
    public Action<T> OnObjectRevoked { get { return m_onObjectRevoked; } set { m_onObjectRevoked = value; } }

    private Action<T> m_onObjectUsed;
    public Action<T> OnObjectUsed { get { return m_onObjectUsed; } set { m_onObjectUsed = value; } }

    public ObjectPool (T obj, int count, Action<T> onObjectAdded)
	{
		m_object = obj;
		m_count = count;
		OnObjectAdded += onObjectAdded;
		m_list = new List<PoolModel> ();
		for (int i = 0; i < m_count; i++) {
			Increase ();		
		}
	}

    public T Get ()
	{
        PoolModel model = m_list.FirstOrDefault (o => !o.IsUsed);
		if (model == null)
			model = Increase ();
		model.IsUsed = true;
        if (OnObjectUsed != null)
            OnObjectUsed.Invoke(model.Object);
        return model.Object;
	}

	public void Revoke (T obj)
	{
		PoolModel model = m_list.FirstOrDefault (o => o.Object == obj);
		if (model != null)
			model.IsUsed = false;
        if (OnObjectRevoked != null)
            OnObjectRevoked.Invoke(obj);
    }

    private PoolModel Increase ()
	{
		T obj = Object.Instantiate<T> (m_object);
		if (OnObjectAdded != null)
			OnObjectAdded.Invoke (obj);
		PoolModel model = new PoolModel (obj);
		m_list.Add (model);
		return model;
	}

	private class PoolModel
	{
		public T Object{ get; set; }

		public bool IsUsed{ get; set; }

		public PoolModel (T obj)
		{
			Object = obj;
		}
	}
}