﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntryBehaviour : MonoBehaviour
{
    [SerializeField]
    private Text m_title;

    [SerializeField]
    private Text m_distance;

    [SerializeField]
    private LayoutElement m_layoutElement;

    [SerializeField]
    private RectTransform m_rectTransform;

    public RectTransform RectTransform { get { return m_rectTransform; } }

    public GeoSearchItem Item { get; set; }

    public void Fill(GeoSearchItem geoSearchItem)
    {
        Item = geoSearchItem;
        m_title.text = Item.Title;
        UpdateDistance();
    }

    public void UpdateDistance()
    {
        Item.Distance = (int)LocationController.Instance.GetDistance(Item.Latitute, Item.Longitude);
        m_distance.text = string.Format("{0}m", Item.Distance);
    }

    public void GoToUrl()
    {
        Application.OpenURL(Item.Url);
    }

    public void Hide()
    {
        LeanTween.value(gameObject, (val) => { m_layoutElement.preferredHeight = val; }, GlobalVars.Instance.EntryHeight, 0, GlobalVars.Instance.AnimationTime)
                 .setOnComplete(() => { gameObject.SetActive(false); });
    }

    public void Show()
    {
        gameObject.SetActive(true);
        LeanTween.value(gameObject, (val) => { m_layoutElement.preferredHeight = val; }, 0, GlobalVars.Instance.EntryHeight, GlobalVars.Instance.AnimationTime);
    }
}
