﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseOperations<T> : SingletonBehaviour<T> where T : BaseOperations<T>
{
    protected IEnumerator WaitForRequest(WWW www, Action<string> onComplete)
    {
        yield return www;
        if (www.error == null)
        {
            onComplete(www.text);
        }
        else
        {
            Debug.Log(www.error);
        }
    }
}
