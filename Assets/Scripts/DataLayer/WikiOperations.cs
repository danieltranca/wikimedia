﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using UnityEngine;

public class WikiOperations : BaseOperations<WikiOperations>
{
    private const string k_apiUrl = "https://en.wikipedia.org/w/api.php?";

    [SerializeField]
    private int m_radius = 10000;

    private Dictionary<int, string> m_urlCache;

    public void Awake()
    {
        m_urlCache = new Dictionary<int, string>();
    }

    private WWW GetApiCall(string request)
    {
        return new WWW(string.Join(string.Empty, new String[] { k_apiUrl, request, "&format=json" }));
    }

    public void GetLocalAttractions(Vector2 userLocation, Action<List<GeoSearchItem>> callback)
    {
        WWW www = GetApiCall(string.Format("action=query&list=geosearch&gsradius={0}&gscoord={1}|{2}", m_radius, userLocation.x, userLocation.y));
        StartCoroutine(WaitForRequest(www, (response) =>
        {
            List<GeoSearchItem> items = new List<GeoSearchItem>();
            JSONArray parsed = JSON.Parse(response)["query"]["geosearch"].AsArray;
            for (int i = 0; i < parsed.Count; i++)
            {
                items.Add(GeoSearchItem.Read(parsed[i]));
            }
            GetUrls(items.Select(i => i.PageId).ToList(), () =>
            {
                for (int i = 0; i < items.Count; i++)
                {
                    items[i].Url = m_urlCache[items[i].PageId];
                }
                if (callback != null)
                    callback(items);
            });
        }));
    }

    private void GetUrls(List<int> pageIds, Action callback)
    {
        string ids = string.Join("|", pageIds.Where(i => !m_urlCache.ContainsKey(i)).Select(i => i.ToString()).ToArray());
        if (string.IsNullOrEmpty(ids))
        {
            if (callback != null)
                callback();
            return;
        }
        WWW www = GetApiCall(string.Format("action=query&prop=info&inprop=url&pageids={0}", ids));
        StartCoroutine(WaitForRequest(www, (response) =>
        {
            List<JSONNode> parsed = JSON.Parse(response)["query"]["pages"].Children.ToList();
            for (int i = 0; i < parsed.Count; i++)
            {
                m_urlCache.Add(parsed[i]["pageid"].AsInt, parsed[i]["fullurl"]);
            }
            if (callback != null)
                callback();
        }));

    }
}